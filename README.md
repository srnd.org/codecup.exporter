# CodeCup Exporter Library (CCEL)


This is the server-side exporter used to send metrics to a data collector like Prometheus. CCEL is compatible with the Prometheus Transit Layer and OpenMetrics Text Representation/ Protocol Buffers.

## Development Enviroment
We use [Pipenv](https://pipenv.readthedocs.io/en/latest/) for quickly creating Virtualenv's with all dependencies installed. Think of it as the NPM of Python, creating a Pipfile and a Pipfile.lock.

 - Install Python 3.7.0, pip, and install pipenv from pip.
 - Run `pipenv install` to create the virtualenv and install dependencies.
 - Run `pipenv shell` to automagically be sourced to the virtualenv.

## Building in development enviroments
We use PyInstaller to build binary executables to deploy to servers. There are limitations of this**.

 - Create a development enviroment by following the catagory called 'Development Enviroment'
 - Run the command `make.sh build` in the root directory.
 - Once it's finished building, a binary file should appear in the root directory.

## Deploying to servers
### DO NOT RUN THIS COMMAND ON YOUR DEVELOPENT MACHINE. YOU HAVE BEEN WARNED.
This process works on Ubuntu/Debian servers (or any OS that uses Systemd)
 - Run `make.sh deploy`
 - ???
 - Done