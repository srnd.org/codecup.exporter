#!/bin/bash

if [ "$1" == "" ]; then
    echo "Missing argument! Specify either build or deploy"
    exit 1
fi

echo "
                   __
  ___ _______  ___/ /
 (_-</ __/ _ \/ _  / 
/___/_/ /_//_/\_,_/  
 _____       _     _____         
|     |___ _| |___|     |_ _ ___ 
|   --| . | . | -_|   --| | | . |
|_____|___|___|___|_____|___|  _|
                            |_|  
"

if [ "$1" == "build" ]; then
    pipenv install --deploy
    pipenv run pyinstaller --noconfirm --onefile --nowindow exporter.spec
    cp dist/exporter exporter_bin
elif [ "$1" == "deploy" ]; then
    pipenv install --deploy
    pipenv run pyinstaller --noconfirm --onefile --nowindow exporter.spec
    read -r -p "Are you sure you want to install CCEL? This will create a systemctl entry and will autostart at launch! Be careful not to do this on your development machine! [y/n] "
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        echo "Copying executable"
        cp dist/exporter /usr/bin/codecup_exporter
        chmod +x /usr/bin/codecup_exporter
        echo "Copying system-deploy"
        cp deployassets/systemd-deploy /etc/systemd/system/codecup_exporter.service
        echo "Adding service"
        systemctl daemon-reload
        systemctl enable codecup_exporter.service
        systemctl start codecup_exporter.service
        echo "Finished"
    fi
elif [ "$1" == "deploy --force" ]; then
    sudo apt install python3-pip
    sudo pip3 install pipenv
    pipenv install --deploy
    pipenv run pyinstaller --noconfirm --onefile --nowindow exporter.spec
    echo "!!WARNING WARNING WARNING !!"
    echo "You've selected autodeploy! Waiting a few seconds in case you have a life changing realization."
    sleep 10s
    cp dist/exporter /usr/bin/codecup_exporter
    chmod +x /usr/bin/codecup_exporter
    echo "Copying system-deploy"
    cp deployassets/systemd-deploy /etc/systemd/system/codecup_exporter.service
    echo "Adding service"
    systemctl daemon-reload
    systemctl enable codecup_exporter.service
    systemctl start codecup_exporter.service
    echo "Finished"
fi