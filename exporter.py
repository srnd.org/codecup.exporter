#!/usr/bin/python3

import sqlalchemy
from sqlalchemy.orm import sessionmaker, scoped_session
import json, decimal, datetime
from prometheus_client import start_http_server, Gauge, Info
import time

teams = [
    "admin",
    "dc",
    "chicago",
    "colorado",
    "blono",
    "boca",
    "virginia",
    "ba",
    "sandiego",
    "la",
    "phoenix",
    "minneapolis",
    "seattle",
    "nashville",
    "dsm",
]

engine = sqlalchemy.create_engine('mysql+pymysql://ctf:ctf@127.0.0.1/fbctf', isolation_level="READ UNCOMMITTED")
# Use when working on local machine
# engine = sqlalchemy.create_engine('mysql+pymysql://root@127.0.0.1/fbctf', isolation_level="READ UNCOMMITTED")
Session = scoped_session(sessionmaker(bind=engine))

def alchemyencoder(obj):
    # JSON encoder function for SQLAlchemy special classes.
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)

def query(query: object) -> object:
    res = json.dumps([dict(r) for r in s.execute(query)], default=alchemyencoder)
    # return all rows as a JSON array of objects
    return json.loads(res)

connections = Gauge('codecup_sessions', 'Literally just counting sessions', ['team_name'])
failures = Gauge('codecup_failures', 'total failures', ['team_name'])
points = Gauge('codecup_points', 'points per team', ['team_name'])
gStatus = Gauge('codecup_status', 'setting statui', ['setting'])

def sessionData():
    for team in teams:
        team_id = query("SELECT id FROM teams where name = '" + team + "';")
        try:
            result = query("SELECT COUNT(*) FROM sessions where team_id = " + str(team_id[0]['id']) + ";")
            connections.labels(team_name=team).set(result[0]['COUNT(*)'])
        except IndexError:
            failures.labels(team_name=team).set('0')

def failureData():
    for team in teams:
        team_id = query("SELECT id FROM teams where name = '" + team + "';")
        try:
            result = query("SELECT COUNT(*) FROM failures_log WHERE team_id = " + str(team_id[0]['id']) + ";")
            failures.labels(team_name=team).set(result[0]['COUNT(*)'])
        except IndexError:
            failures.labels(team_name=team).set('0')

def pointData():
    for team in teams:
        team_id = query("SELECT id FROM teams where name = '" + team + "';")
        try:
            result = query("SELECT points FROM teams WHERE id = " + str(team_id[0]['id']) + ";")
            points.labels(team_name=team).set(result[0]['points'])
        except IndexError:
            points.labels(team_name=team).set("0")

def updateGameStatus():

    # Scoring setting
    if query("SELECT value FROM configuration WHERE field = 'scoring';")[0]["value"] != '1':
        gStatus.labels(setting='score_status').set('2')
    else:
        gStatus.labels(setting='score_status').set('0')

    # Timer status
    if query("SELECT value FROM configuration WHERE field = 'timer';")[0]["value"] != "1":
        gStatus.labels(setting='timer_status').set("4")
    else:
        gStatus.labels(setting='timer_status').set("0")

    # Paused status
    if query("SELECT value FROM configuration WHERE field = 'game_paused';")[0]["value"] == '1':
        gStatus.labels(setting='pause_status').set('6')
    else:
        gStatus.labels(setting='pause_status').set('0')

    # Game status
    if query("SELECT value FROM configuration WHERE field = 'game';")[0]["value"] != "1":
        gStatus.labels(setting='game_status').set('8')
        gStatus.labels(setting='pause_status').set('8')
    else:
        gStatus.labels(setting='game_status').set('0')

if __name__ == '__main__':
    start_http_server(4060)
    while True:
        s = Session()
        sessionData()
        failureData()
        pointData()
        updateGameStatus()
        s.close()
        time.sleep(5)